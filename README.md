# tabela

**Simple app that shows CSV content in table.**

This app is made only for university purposes. Tabela shows content from CSV file in table. You can also add new rows and save table in CSV or XML.

License: AGPL-3.0-or-later
Author: Grzegorz Cichocki
