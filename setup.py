from setuptools import setup

package_name = 'tabela'
filename = 'tabela/__init__.py'

setup(
    name='tabela',
    version='1.0',
    description='Simple app that shows CSV content in table.',
    author='cichy1173',
    author_email='grzegorz@cichy1173.eu',
    packages=['tabela'],
    install_requires=[
        'PySide6==6.5.0',  
    ],
    entry_points={
        'console_scripts': [
            'tabela=tabela.main:main',  
        ],
    },
)