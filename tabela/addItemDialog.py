from PySide6.QtWidgets import QDialog, QLabel, QLineEdit, QPushButton, QVBoxLayout

class AddItemDialog(QDialog):
    def __init__(self, headers, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Add Item")
        self.headers = headers
        self.edits = []

        # create layout
        layout = QVBoxLayout()

        for header in self.headers:
            label = QLabel(header)
            edit = QLineEdit()
            self.edits.append(edit)

            layout.addWidget(label)
            layout.addWidget(edit)

        # create button box
        button_box = QPushButton("Add")
        button_box.clicked.connect(self.accept)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.reject)

        button_layout = QVBoxLayout()
        button_layout.addWidget(button_box)
        button_layout.addWidget(cancel_button)

        layout.addLayout(button_layout)
        self.setLayout(layout)

    def get_data(self):
        # retrieve the data entered by the user and return it as a list
        return [edit.text() for edit in self.edits]
