from PySide6.QtWidgets import QApplication,QMainWindow,QFileDialog, QTableView, QAbstractItemView, QMessageBox
from PySide6.QtCore import Qt
from tabela.fileProcessing import *
from tabela.myTableModel import MyTableModel
from tabela.addItemDialog import AddItemDialog
from tabela.saveDataDialog import SaveDataDialog


global data
data = []

class MainWindow(QMainWindow):
    def __init__(self, app):
        super().__init__()
        self.app = app
        self.setWindowTitle("Tabela")
        self.resize(800,600)

        #menu bar
        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu("File")

        ##Reading file

        pick_file_action = file_menu.addAction("Open CSV")
        pick_file_action.triggered.connect(self.open_file)

        ##Saving file
        
        save_csv_action = file_menu.addAction("Save as CSV")
        save_csv_action.triggered.connect(self.save_csv)

        save_xml_action = file_menu.addAction("Save as XML")
        save_xml_action.triggered.connect(self.save_xml)

        ##Adding new item

        add_item_action = file_menu.addAction("Add item")
        add_item_action.triggered.connect(self.add_item)

        ##Quit

        quit_action = file_menu.addAction("Quit")
        quit_action.triggered.connect(self.quit_app)

        # create table view

        self.table_view = QTableView(self)
        self.setCentralWidget(self.table_view)

    def quit_app(self):
        self.app.quit()

    
    def open_file(self):

        global data
        file = QFileDialog.getOpenFileName(self)
        if file:
            data = copyDataFromCSV(file[0])
            headers = data[0]
            dataWithoutHeaders = data[1:]
            model = MyTableModel(dataWithoutHeaders, headers)
            self.table_view.setModel(model)


    def add_item(self):
        global data
        dialog = AddItemDialog(data[0], self)  # Przekazujemy nagłówki jako argument
        if dialog.exec_() == AddItemDialog.Accepted:
            newData = dialog.get_data()
            print(newData)

            data.append(newData)
            model = MyTableModel(data, data[0])  # Przekazujemy nagłówki jako argument
            self.table_view.setModel(model)


    def save_csv(self): 
        global data

        dialog = SaveDataDialog(self)   

        if dialog.exec_() == SaveDataDialog.Accepted:

            if data:
                fileName = dialog.get_data() + ".csv"
                savingFileWithCsvFormat(fileName, data)
            else:
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Warning)
                message_box.setText("Data is empty")
                message_box.exec_() 

    def save_xml(self):
        global data
        
        dialog = SaveDataDialog(self)

        if dialog.exec_() == SaveDataDialog.Accepted:

            if data:
                fileName = dialog.get_data() + ".xml"
                saveFileAsXML(data, fileName, data[0])
            else:
                message_box = QMessageBox()
                message_box.setIcon(QMessageBox.Warning)
                message_box.setText("Data is empty")
                message_box.exec_() 

