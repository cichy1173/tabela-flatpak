from PySide6.QtWidgets import QDialog, QLabel, QLineEdit, QPushButton, QVBoxLayout

class SaveDataDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Save file")

        # create labels and line edits for each field

        fileName_label = QLabel("Filename:")
        self.fileName_edit = QLineEdit()

      
        # create button box
        button_box = QPushButton("Save")
        button_box.clicked.connect(self.accept)
        cancel_button = QPushButton("Cancel")
        cancel_button.clicked.connect(self.reject)

        # create layout
        layout = QVBoxLayout()
        layout.addWidget(fileName_label)
        layout.addWidget(self.fileName_edit)

        button_layout = QVBoxLayout()
        button_layout.addWidget(button_box)
        button_layout.addWidget(cancel_button)

        layout.addLayout(button_layout)

        self.setLayout(layout)

    def get_data(self):
        # retrieve the data as a String
        return self.fileName_edit.text()
